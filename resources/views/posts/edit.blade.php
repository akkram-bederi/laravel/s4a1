@extends('layouts.app')

@section('content')
	<h1>Edit Post</h1>
	<form action="{{ action('PostController@update',[$posts->id]) }}" method="POST">
		@csrf 
		{{-- Hidden input to change method to PUT --}}
		<input type="hidden" name="_method" value="PUT">
		<div class="form-group">
			<label for="title-input">Title</label>
			<input id="title-input" type="text" name="title" class="form-control" placeholder="title" value="{{$posts->title}}">
		</div>

		<div class="form-group">
			<label for="body-input">Body</label>
			<textarea id="body-input" class="form-control" placeholder="Body" rows="5">{{$posts->body}}</textarea>
		</div>

		<button type="submit" class="btn btn-primary">Submit</button>
	</form>
@endsection