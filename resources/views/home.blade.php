@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <h2>My Posts</h2>
                @if(count($posts)>0)
                    <table class="table table-striped">
                        <tr>
                            <th>Title</th>
                            <th></th>
                            <th></th>
                        </tr>
                        @foreach($posts as $post)
                        <tr>
                            <td>{{$post->title}}</td>
                            <td><a href="/posts/{{$post->id}}/edit" class="btn btn-primary">Edit</a></td>
                            <td><form action="{{action('PostController@destroy', [$post->id])}}" method="POST">
                                @csrf
                                <input type="hidden" name="_method" value="DELETE">
                                <button class="btn btn-danger mx-3">Delete</button>
                                </form></td>
                        </tr>
                        @endforeach
                    </table>
                @else
                    <p>No posts found</p>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
